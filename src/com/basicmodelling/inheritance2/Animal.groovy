package com.basicmodelling.inheritance2

interface Animal {
    
    public String eat()
    
    public String makeSound()
    
}
