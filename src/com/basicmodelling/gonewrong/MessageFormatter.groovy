package com.basicmodelling.gonewrong


class MessageFormatter {
    
    public String formatMessageAsHtml(Message message) {
        return """<!DOCTYPE html>
<html>
    <head>
        <title>${message.title}</title>
    </head>
    <body>
  
        <h1>${message.title}</h1>
        <p>${message.text}</p>
    
    </body>
</html>"""
    }
    
    public String formatMessageAsPlain(Message message) {
        return "${message.title}: ${message.text}"
    }
    
    public String formatMessageAsJson(Message message) {
        return """{
    "type": "message",
    "attributes": [
         "title" : ${message.title},
         "text" : ${message.text}
     ]
}"""  
    }
    
    public String formatMessageAsXml(Message message) {
        return """<?xml version="1.0" encoding="UTF-8"?>
<message>
    <title>${message.title}</title>
    <text>${message.text}</text>
</message>"""
    }
    
}
