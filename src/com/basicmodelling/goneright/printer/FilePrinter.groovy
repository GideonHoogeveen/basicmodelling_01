package com.basicmodelling.goneright.printer

import com.basicmodelling.goneright.message.Message
import com.basicmodelling.goneright.message.MessageFormat



class FilePrinter implements Printer {
    
    String filePath
    
    public FilePrinter(String filePath) {
        this.filePath = filePath
    }

    @Override
    public void printMessage(Message message, MessageFormat format) {
        
        assert filePath
        assert message
        assert format
        
        File file = new File(filePath)        
        file.write format.formatMessage(message)
    }

}
