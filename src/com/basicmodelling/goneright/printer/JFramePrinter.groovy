package com.basicmodelling.goneright.printer

import java.awt.BorderLayout

import javax.swing.JFrame
import javax.swing.JLabel

import com.basicmodelling.goneright.message.Message
import com.basicmodelling.goneright.message.MessageFormat

class JFramePrinter implements Printer {

    @Override
    public void printMessage(Message message, MessageFormat format) {
        JFrame frame = new JFrame("FrameDemo")
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
        
        JLabel label = new JLabel()
        label.setText(format.formatMessage(message))
        label.setLocation(0, 0)
        
        frame.getContentPane().add(label, BorderLayout.CENTER)
        frame.pack()
        frame.setVisible(true)
    }

}
