package com.basicmodelling.goneright.printer

import com.basicmodelling.goneright.message.Message
import com.basicmodelling.goneright.message.MessageFormat



class ConsolePrinter implements Printer {
    
    @Override
    public void printMessage(Message message, MessageFormat format) {
        assert message
        assert format
        println format.formatMessage(message)
    }

}
