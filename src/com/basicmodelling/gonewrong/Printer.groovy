package com.basicmodelling.gonewrong

import java.awt.BorderLayout

import javax.swing.JFrame
import javax.swing.JLabel

class Printer {
    
    MessageFormatter messageFormatter
    
    public Printer() {
        this.messageFormatter = new MessageFormatter()
    }
    
    public void printMessageToFile(Message message, FormatType formatType, String filePath) {
        assert filePath
        assert formatType
        assert message
        
        File file = new File(filePath)
        
        switch(formatType) {
            case FormatType.HTML:
                file.write messageFormatter.formatMessageAsHtml(message)
                break;
            case FormatType.XML:
                file.write messageFormatter.formatMessageAsXml(message)
                break;
            case FormatType.JSON:
                file.write messageFormatter.formatMessageAsJson(message)
                break;
            case FormatType.PLAIN:
                file.write messageFormatter.formatMessageAsPlain(message)
                break;
        }
    }
    
    public void printMessageToConsole(Message message, FormatType formatType) {
        assert formatType
        assert message
        
        switch(formatType) {
            case FormatType.HTML:
                println messageFormatter.formatMessageAsHtml(message)
                break;
            case FormatType.XML:
                println messageFormatter.formatMessageAsXml(message)
                break;
            case FormatType.JSON:
                println messageFormatter.formatMessageAsJson(message)
                break;
            case FormatType.PLAIN:
                println messageFormatter.formatMessageAsPlain(message)
                break;
        }
    }
    
    public void printMessageToJFrame(Message message, FormatType formatType) {
        JFrame frame = new JFrame("FrameDemo")
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
        
        JLabel label = new JLabel()
        
        switch(formatType) {
            case FormatType.HTML:
                label.setText(messageFormatter.formatMessageAsHtml(message))
                break;
            case FormatType.XML:
                label.setText(messageFormatter.formatMessageAsXml(message))
                break;
            case FormatType.JSON:
                label.setText(messageFormatter.formatMessageAsJson(message))
                break;
            case FormatType.PLAIN:
                label.setText(messageFormatter.formatMessageAsPlain(message))
                break;
        }
        
        label.setLocation(0, 0)
        
        frame.getContentPane().add(label, BorderLayout.CENTER)
        frame.pack()
        frame.setVisible(true)
    }
    
    
}
