package com.basicmodelling.version2

class HtmlMessageFormat {
    
    public String formatMessageAsHtml(Message message) {
        return """<!DOCTYPE html>
<html>
    <head>
        <title>${message.title}</title>
    </head>
    <body>
  
        <h1>${message.title}</h1>
        <p>${message.text}</p>
    
    </body>
</html>"""
    }
    
}
