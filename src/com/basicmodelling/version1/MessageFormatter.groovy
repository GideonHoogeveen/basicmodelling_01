package com.basicmodelling.version1


class MessageFormatter {
    
    public String formatMessageAsHtml(Message message) {
        return """<!DOCTYPE html>
<html>
    <head>
        <title>${message.title}</title>
    </head>
    <body>
  
        <h1>${message.title}</h1>
        <p>${message.text}</p>
    
    </body>
</html>"""
    }
    
    public String formatMessageAsPlain(Message message) {
        return "${message.title}: ${message.text}"
    }
    
}
