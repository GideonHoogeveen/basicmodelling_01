package com.basicmodelling.gonewrong

enum FormatType {
    JSON, HTML, XML, PLAIN
}
