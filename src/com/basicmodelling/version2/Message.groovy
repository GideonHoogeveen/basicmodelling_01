package com.basicmodelling.version2

class Message {
    
    String title
    String text
    
    public Message(String title, String text) {
        this.title = title
        this.text = text
    }
    
}
