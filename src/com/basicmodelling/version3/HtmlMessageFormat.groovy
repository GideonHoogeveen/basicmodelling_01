package com.basicmodelling.version3

class HtmlMessageFormat implements MessageFormat {
    
    public String formatMessage(Message message) {
        return """<!DOCTYPE html>
<html>
    <head>
        <title>${message.title}</title>
    </head>
    <body>
  
        <h1>${message.title}</h1>
        <p>${message.text}</p>
    
    </body>
</html>"""
    }
    
}
