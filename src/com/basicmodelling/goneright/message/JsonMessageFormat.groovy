package com.basicmodelling.goneright.message

class JsonMessageFormat implements MessageFormat {

    @Override
    public String formatMessage(Message message) {
        return """{
    "type": "message",
    "attributes": [
         "title" : ${message.title},
         "text" : ${message.text}
     ]
}"""  
    }

}
