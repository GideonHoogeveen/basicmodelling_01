package com.basicmodelling.version3


class JsonMessageFormat implements MessageFormat {
    
    public String formatMessage(Message message) {
        return """{
    "type": "message",
    "attributes": [
         "title" : ${message.title},
         "text" : ${message.text}
     ]
}"""  
    }
    
}
