package com.basicmodelling.version1

import java.awt.BorderLayout

import javax.swing.JFrame
import javax.swing.JLabel

class Printer {
    
    MessageFormatter messageFormatter
    
    public Printer() {
        this.messageFormatter = new MessageFormatter()
    }
    
    public void printMessageToConsole(Message message, FormatType formatType) {
        assert formatType
        assert message
        
        switch(formatType) {
            case FormatType.HTML:
                println messageFormatter.formatMessageAsHtml(message)
                break;
            case FormatType.PLAIN:
                println messageFormatter.formatMessageAsPlain(message)
                break;
        }
    }
    
}
