package com.basicmodelling.version3

class PlainMessageFormat implements MessageFormat {
    
    public String formatMessage(Message message) {
        return "${message.title}: ${message.text}"
    }
    
}
