package com.basicmodelling.inheritance2

class Dog implements Animal {
    
    @Override
    public String makeSound() {
        return "wooof"
    }

    @Override
    public String eat() {
        return "om nom nom nom";
    }
    
}
