package com.basicmodelling.version3

import java.awt.BorderLayout

import javax.swing.JFrame
import javax.swing.JLabel

class ConsolePrinter {
    
    public void printMessageToConsole(Message message, MessageFormat format) {
        assert message
        assert format
        
        println format.formatMessage(message)
    }
    
    
    
}
