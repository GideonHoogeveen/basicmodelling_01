package com.basicmodelling.version2


class JsonMessageFormat {
    
    public String formatMessageAsJson(Message message) {
        return """{
    "type": "message",
    "attributes": [
         "title" : ${message.title},
         "text" : ${message.text}
     ]
}"""  
    }
    
}
