package com.basicmodelling.composition

class Bolt implements Part {

    @Override
    public String getPartInformation() {
        return "small simple bolt";
    }

}
