package com.basicmodelling.goneright.message

class Message {
    
    String title
    String text
    
    public Message(String title, String text) {
        this.title = title
        this.text = text
    }
    
}
