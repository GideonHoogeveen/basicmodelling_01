package com.basicmodelling.inheritance1

abstract class Animal {
    
    public String eat() {
        return "Om nom nom nom"
    }
    
    public abstract String makeSound()
    
}
