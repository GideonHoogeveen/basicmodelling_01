package com.basicmodelling.version2

class PrinterTest {
    
    public static void main(String[] args) {
        Message message = new Message('Mijn tekst', 'Dit is een hele mooie tekst waar ik ontzettend trots op ben.');
        
        Printer printer = new Printer()
        printer.printMessageToConsole(message, new JsonMessageFormat())
        println '\n---------------\n'
        printer.printMessageToConsole(message, new XmlMessageFormat())
        println '\n---------------\n'
        printer.printMessageToConsole(message, new PlainMessageFormat())
        println '\n---------------\n'
        printer.printMessageToConsole(message, new HtmlMessageFormat())
        
    }
    
}
