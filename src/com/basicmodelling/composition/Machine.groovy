package com.basicmodelling.composition

class Machine {

    List<Part> parts = []
    
    public void addPart(Part part) {
        parts << part
    }
    
    public void listAllParts() {
        parts.each { Part part ->
            println part.getPartInformation()
            println '\n---------------\n'
        }
    }
    
}
