package com.basicmodelling.version3

class PrinterTest {
    
    public static void main(String[] args) {
        Message message = new Message('Mijn tekst', 'Dit is een hele mooie tekst waar ik ontzettend trots op ben.');
        
        JsonMessageFormat jsonFormat = new JsonMessageFormat()
        XmlMessageFormat xmlFormat = new XmlMessageFormat()
        PlainMessageFormat plainFormat = new PlainMessageFormat()
        HtmlMessageFormat htmlFormat = new HtmlMessageFormat()
        
        ConsolePrinter consolePrinter = new ConsolePrinter()
        consolePrinter.printMessageToConsole(message, jsonFormat)
        println '\n---------------\n'
        consolePrinter.printMessageToConsole(message, xmlFormat)
        println '\n---------------\n'
        consolePrinter.printMessageToConsole(message, plainFormat)
        println '\n---------------\n'
        consolePrinter.printMessageToConsole(message, htmlFormat)
        
        FilePrinter filePrinter = new FilePrinter("D:/output/output.json")
        filePrinter.printMessageToFile(message, jsonFormat)
        filePrinter = new FilePrinter("D:/output/output.xml")
        filePrinter.printMessageToFile(message, xmlFormat)
        filePrinter = new FilePrinter("D:/output/output.txt")
        filePrinter.printMessageToFile(message, plainFormat)
        filePrinter = new FilePrinter("D:/output/output.html")
        filePrinter.printMessageToFile(message, htmlFormat)
        
        
        JFramePrinter jframePrinter = new JFramePrinter()
        jframePrinter.printMessageToJFrame(message, jsonFormat)
        jframePrinter.printMessageToJFrame(message, xmlFormat)
        jframePrinter.printMessageToJFrame(message, plainFormat)
        jframePrinter.printMessageToJFrame(message, htmlFormat)
        
    }
    
}
