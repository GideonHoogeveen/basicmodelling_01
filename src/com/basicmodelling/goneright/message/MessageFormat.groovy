package com.basicmodelling.goneright.message

interface MessageFormat {
    
    public String formatMessage(Message message)
    
}
