package com.basicmodelling.goneright.printer

import com.basicmodelling.goneright.message.Message
import com.basicmodelling.goneright.message.MessageFormat



interface Printer {
    
    public void printMessage(Message message, MessageFormat format)

}
