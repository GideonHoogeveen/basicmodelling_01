package com.basicmodelling.composition

class MachineTest {

    public static void main(String[] args) {
        Machine machine = new Machine()
        
        machine.addPart(new Bolt())
        machine.addPart(new Cable())
        machine.addPart(new Chip())
        
        machine.listAllParts()
    }

}
