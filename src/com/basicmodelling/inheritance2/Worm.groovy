package com.basicmodelling.inheritance2

class Worm implements Animal {
    
    @Override
    public String eat() {
        return "Crisp crisp crisp"
    }
    
    @Override
    public String makeSound() {
        return "..."
    }
}
