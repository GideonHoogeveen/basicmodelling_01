package com.basicmodelling.version2

import java.awt.BorderLayout

import javax.swing.JFrame
import javax.swing.JLabel

class Printer {
    
    public void printMessageToConsole(Message message, JsonMessageFormat format) {
        assert message
        assert format
        
        println format.formatMessageAsJson(message)
    }
    
    public void printMessageToConsole(Message message, XmlMessageFormat format) {
        assert message
        assert format
        
        println format.formatMessageAsXml(message)
    }
    
    public void printMessageToConsole(Message message, PlainMessageFormat format) {
        assert message
        assert format
        
        println format.formatMessageAsPlain(message)
    }
    
    public void printMessageToConsole(Message message, HtmlMessageFormat format) {
        assert message
        assert format
        
        println format.formatMessageAsHtml(message)
    }
    
}
