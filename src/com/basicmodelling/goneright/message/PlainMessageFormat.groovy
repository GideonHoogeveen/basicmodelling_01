package com.basicmodelling.goneright.message

class PlainMessageFormat implements MessageFormat {

    @Override
    public String formatMessage(Message message) {
        return "${message.title}: ${message.text}"
    }
    
}
