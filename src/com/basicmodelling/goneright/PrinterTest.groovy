package com.basicmodelling.goneright

import com.basicmodelling.goneright.message.HtmlMessageFormat
import com.basicmodelling.goneright.message.JsonMessageFormat
import com.basicmodelling.goneright.message.Message
import com.basicmodelling.goneright.message.MessageFormat
import com.basicmodelling.goneright.message.PlainMessageFormat
import com.basicmodelling.goneright.message.XmlMessageFormat
import com.basicmodelling.goneright.printer.ConsolePrinter
import com.basicmodelling.goneright.printer.FilePrinter
import com.basicmodelling.goneright.printer.JFramePrinter
import com.basicmodelling.goneright.printer.Printer



class PrinterTest {

    public static void main(String[] args) {
        Message message = new Message( 'Mijn tekst', 'Dit is een hele mooie tekst waar ik ontzettend trots op ben.')
        
        MessageFormat htmlFormat = new HtmlMessageFormat()
        MessageFormat xmlFormat = new XmlMessageFormat()
        MessageFormat jsonFormat = new JsonMessageFormat()
        MessageFormat plainFormat = new PlainMessageFormat()
        
        Printer consolePrinter = new ConsolePrinter()
        
        consolePrinter.printMessage(message, htmlFormat)
        println '\n----------------\n'
        consolePrinter.printMessage(message, xmlFormat)
        println '\n----------------\n'
        consolePrinter.printMessage(message, jsonFormat)
        println '\n----------------\n'
        consolePrinter.printMessage(message, plainFormat)
        
        Printer htmlPrinter = new FilePrinter("D:/output/output.html")
        htmlPrinter.printMessage(message, htmlFormat)
        
        Printer xmlPrinter = new FilePrinter("D:/output/output.xml")
        xmlPrinter.printMessage(message, xmlFormat)
        
        Printer jsonPrinter = new FilePrinter("D:/output/output.json")
        jsonPrinter.printMessage(message, jsonFormat)
        
        Printer plainPrinter = new FilePrinter("D:/output/output.txt")
        plainPrinter.printMessage(message, plainFormat)
        
        Printer framePrinter = new JFramePrinter()
        framePrinter.printMessage(message, jsonFormat)
        framePrinter.printMessage(message, htmlFormat)
        framePrinter.printMessage(message, xmlFormat)
        framePrinter.printMessage(message, plainFormat)
    }
    
}
