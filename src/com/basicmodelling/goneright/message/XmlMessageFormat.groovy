package com.basicmodelling.goneright.message

class XmlMessageFormat implements MessageFormat {

    @Override
    public String formatMessage(Message message) {
        return """<?xml version="1.0" encoding="UTF-8"?>
<message>
    <title>${message.title}</title>
    <text>${message.text}</text>
</message>"""
    }

}
