package com.basicmodelling.composition

class Chip implements Part {

    @Override
    public String getPartInformation() {
        return "NFC chip\nnot formatted\ntype: NTAG213\nstorage: 1024 bytes"
    }

}
