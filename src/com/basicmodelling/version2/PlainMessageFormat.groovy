package com.basicmodelling.version2

class PlainMessageFormat {
    
    public String formatMessageAsPlain(Message message) {
        return "${message.title}: ${message.text}"
    }
    
}
