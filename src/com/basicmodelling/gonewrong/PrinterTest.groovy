package com.basicmodelling.gonewrong

class PrinterTest {
    
    public static void main(String[] args) {
        Message message = new Message('Mijn tekst', 'Dit is een hele mooie tekst waar ik ontzettend trots op ben.');
        
        Printer printer = new Printer()
        printer.printMessageToConsole(message, FormatType.HTML)
        printer.printMessageToJFrame(message, FormatType.PLAIN)
        
    }
    
}
