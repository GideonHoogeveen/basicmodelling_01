package com.basicmodelling.inheritance2

class AnimalTest {

    public static void main(String[] args) {
        Animal dog = new Dog()
        Animal worm = new Worm()
        
        println dog.eat()
        println dog.makeSound()
        
        println worm.eat()
        println worm.makeSound()
    }

}
