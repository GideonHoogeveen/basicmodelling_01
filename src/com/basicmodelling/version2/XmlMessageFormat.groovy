package com.basicmodelling.version2


class XmlMessageFormat {
    
    public String formatMessageAsXml(Message message) {
        return """<?xml version="1.0" encoding="UTF-8"?>
<message>
    <title>${message.title}</title>
    <text>${message.text}</text>
</message>"""
    }
    
}
