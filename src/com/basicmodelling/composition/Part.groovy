package com.basicmodelling.composition

interface Part {
    
    public String getPartInformation()
    
}
