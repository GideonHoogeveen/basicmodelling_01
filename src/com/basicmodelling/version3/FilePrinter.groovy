package com.basicmodelling.version3


class FilePrinter {
    
    String filePath
    
    public FilePrinter(String filePath) {
        this.filePath = filePath
    }

    public void printMessageToFile(Message message, MessageFormat format) {
        
        assert filePath
        assert message
        assert format
        
        File file = new File(filePath)        
        file.write format.formatMessage(message)
    }
    
    
    
}
