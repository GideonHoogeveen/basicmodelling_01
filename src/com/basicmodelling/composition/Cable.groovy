package com.basicmodelling.composition

class Cable implements Part {

    @Override
    public String getPartInformation() {
        return "Copper Cable"
    }

}
