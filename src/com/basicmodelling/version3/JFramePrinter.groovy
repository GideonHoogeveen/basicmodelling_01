package com.basicmodelling.version3

import java.awt.BorderLayout

import javax.swing.JFrame
import javax.swing.JLabel

class JFramePrinter {
    
    public void printMessageToJFrame(Message message, MessageFormat format) {
        JFrame frame = new JFrame("FrameDemo")
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
        
        JLabel label = new JLabel()
        label.setText(format.formatMessage(message))
        label.setLocation(0, 0)
        
        frame.getContentPane().add(label, BorderLayout.CENTER)
        frame.pack()
        frame.setVisible(true)
    }
    
}
